FROM node
RUN mkdir /code
WORKDIR /code
ADD . /code/
CMD npm run build
