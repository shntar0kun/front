import {
  saveFields,
} from './communication';

const actions = {
  saveFields,
};

export default actions;
